import setuptools
from pathlib import Path


about = {}
with (Path().cwd() / 'force' / '__version__.py').open() as version_file:
    exec(version_file.read(), about)

with Path("README.md").open() as readme_file:
    readme = readme_file.read()


setuptools.setup(
    name=about['__title__'],
    version=about['__version__'],
    author=about['__author__'],
    description=about['__description__'],
    long_description=readme,
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            'sample-python-app = sample_python_app:app.main'
        ]
    }
)
