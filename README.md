# Sample Python App

This is an example of how to structure a python application so it is deployable on the Vader platform.  This structure allows your python code to be built into a wheel file which can be installed onto another machine using pip.  Once installed on another machine the code can executed through the command line interface or imported into another python project.

## High Level Structure
```
project
|___project
|   |   __init__.py
|   |   app.py
|   |   module_1.py
|
|___tests
|   |   test_file_1.py
|
|   README.md
|   requirements.txt
|   setup.py
```

## Executable Command Line App

When packaging your python application we will configure an entry point for a command line application.  This is handled in the `setup.py` in the root of your project in the `entry_points` parameter.  Once installed this tells python to execute `main` function in the `app.py` module.  All of the features you want to expose through the command line should begin at `main` in `app.py`.  This will be how a production process will start your python application to execute it's functionality.

setup.py
```python
    entry_points={
        'console_scripts': [
            'sample-python-app = sample_python_app:app.main'
        ]
    }
```

app.py
```python
import argparse
from .work_goes_here import do_work

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("work", help="What work should I be doing?")
    args = parser.parse_args()

    do_work(args.work)


if __name__ == "__main__":
    main()
```

```console
curtoale$ sample-python-app --help
usage: sample-python-app [-h] work

positional arguments:
  work        What work should I be doing?

optional arguments:
  -h, --help  show this help message and exit

curtoale$ sample-python-app "calibrate my models"
Working on calibrate my models....
```


## Python Library

In addition to being a command line application your python code can be imported as a package in other python projects.

```python
from sample_python_app.work_goes_here import do_work

do_work("running projections")
```

## Testing
Unit tests should be written under the `tests` directory in your project.  The [pytest](https://docs.pytest.org/en/stable/) library is recommended for writing unit tests.

test_work_goes_here.py
```python
import pytest
from sample_python_app.work_goes_here import do_work


def test_do_work():
    assert do_work("calibrating models") == 42
```

```console
curtoale$ pytest
=================================== test session starts ===================================
platform darwin -- Python 3.9.1, pytest-6.2.1, py-1.10.0, pluggy-0.13.1
rootdir: /Users/curtoale/Projects/sample_python_app
collected 1 item                                                                                                                                                                                                                                                               

tests/test_work_goes_here.py .                                                                                                                                                                                                                                           [100%]

=================================== 1 passed in 1.02s ===================================

```

## Building a Wheel File From Your Project
In the root of your project execute
```console
curtoale$ pip wheel .
```

This will generate .whl file which can be used to install your python app.