import time


def do_work(work: str) -> int:
    print(f"Working on {work}....")
    time.sleep(1)
    return 42
