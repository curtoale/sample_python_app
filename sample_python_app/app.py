import argparse
from .work_goes_here import do_work


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("work", help="What work should I be doing?")
    args = parser.parse_args()

    do_work(args.work)


if __name__ == "__main__":
    main()